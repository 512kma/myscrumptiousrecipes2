from django.contrib import admin
from .models import Recipe, RecipeStep, Ingredients
# Register your models here.


# @admin.register(Recipe)     ## either one of these work
# admin.site.register(Recipe)

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "title", "description", "created_on", "id"
    )


@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_displays = ('step_number',
                     'instruction',
                     'id'
                     )


@admin.register(Ingredients)
class Ingredients(admin.ModelAdmin):
    list_displays = (
        'Amount',
        'Food item',
    )
